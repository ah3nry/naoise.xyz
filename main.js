import './style.css'
import { setupCounter } from './counter.js'

document.querySelector('#app').innerHTML = `
  <main class="full-height center-items">
    <section class="container">
      <h1 class="title">Hi! I'm Naoise.</h1>
  </main>
`

setupCounter(document.querySelector('#counter'))
